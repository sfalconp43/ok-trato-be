'use strict'

const { ReportCategory } = require('../../models/reportCategory');

let controller = {

    /**
    * @api {get} /get-report-categorie
    * @apiName ReportCategory
    * @apiGroup ReportCategory
    *
    * Consultar todas las categorias por tipo
    * 
    * @apiSuccess (201) {Object} mixed `Report` object
    */
    getReportCategory: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //Buscar reportes por tipo
        let reportCategory = await ReportCategory.find({type: parameters.type});
    
        if (!reportCategory) {
            return response.status(400).send({
                status:false,
                message: `Error consultando categorias`,
            })
        }else{
            return response.status(201).send({
                status:true,
                message: `Consulta exitosa`,
                reportCategory
            })
        }

    },

    /**
    * Insertar categoria de reporte
    */
    insertCategoryReport: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //crear data para insertar
        var data = {
            description: parameters.description,
            type: parameters.type,
        };
        
        //registrar reporte 
        var category = new ReportCategory(data);

        const categoryStored = await category.save();

        if (!categoryStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando categoria`,
            })
        }

        //reporte registrado
        return response.status(201).send({ 
            status:true,  
            message: `Categoria registrada correctamente`, 
            id: categoryStored._id 
        })

    },


    /**
    * seed categoria de reporte
    */
    seedCategoryReport: async (request, response) => {

        //crear data para insertar
        //categorias de reportes 0:usuario/1:producto
        var insert = [
        { description: 'No asistió a la reunión', type: 0 },
        { description: 'Hizo una oferta baja', type: 0 },
        { description: 'Grosero o inapropiado', type: 0 },
        { description: 'Problema con los productos', type: 0 },
        { description: 'Problemas para encontrarse', type: 0 },
        { description: 'Problemas con los mensajes', type: 0 },
        { description: 'Problema de envío', type: 0 },
        { description: 'Otro', type: 0 },
        { description: 'Prohibido en Ok Trato', type: 1 },
        { description: 'Ofensivo o inapropiado', type: 1 },
        { description: 'Publicación duplicada', type: 1 },
        { description: 'Categoría incorrecta', type: 1 },
        { description: 'Parece una estafa', type: 1 },
        { description: 'Creo que es robado', type: 1 },
        { description: 'Problema con el envío', type: 1 },
        { description: 'Otro', type: 1 }, 
        ];

        const categoryStored = await ReportCategory.insertMany(insert);

        if (!categoryStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando batch`,
            })
        }

        //reporte registrado
        return response.status(201).send({ 
            status:true,  
            message: `batch registrado correctamente`, 
            id: categoryStored._id 
        })

    },

}

module.exports = controller;