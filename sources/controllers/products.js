'use strict'

const config = require('../../config/config');
const { Product } = require('../../models/product');
const { Board } = require('../../models/board');
const { StatsProduct } = require('../../models/statsProducts');
//Underscore
const _ = require('underscore');
const fs = require('fs');//archivos
const path = require('path');//rutas

let controller = {

    /**
    * @api {put} /insert-chat
    * @apiName Products
    * @apiGroup Products
    *
    * Insertar producto por usuario
    * 
    * @apiSuccess (201) {Object} mixed `product` object
    */
    insertProduct: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;
        //imagenes
        var files = request.files;

        //Crear directorio si no existe
        let pathProduct = path.resolve(__dirname, `../../public/uploads/products/${ parameters.iduser }`);
        if (!fs.existsSync(pathProduct)){
            fs.mkdirSync(pathProduct);
        }

        if(files){

            var images=[];

            files.forEach( (file, idx) => {

                //obtener nombre de imagen original
                let image = file.originalname;
                //obtener nombre del archivo cargado
                let imageProduct = file.filename;
                let splitName = image.split('.');
                //obtener extension del archivo
                let extension = splitName[splitName.length - 1];
                //renombrar archivo
                let newName = `pd-${ idx  }-${ new Date().getTime()  }.${ extension }`;
                
                //obtener ruta de archivo
                let pathImage = path.resolve(__dirname, `../../public/uploads/products`);

                //ruta final carpeta del usuario
                let endPathImage = path.resolve(__dirname, `../../public/uploads/products/${ parameters.iduser  }`);

                //renombrar y mover
                fs.renameSync(pathImage+'/'+imageProduct, endPathImage+'/'+newName);

                let route = `products/${ parameters.iduser  }/`;
                images.push(`${route}${newName}`);

            });

            //registrar producto 
            var product = new Product({
                iduser:parameters.iduser,
                idcategory:parameters.idcategory,
                title:parameters.title,
                description:parameters.description,
                price:parameters.price,
                flexible:parameters.flexible,
                location: {
                    type: "Point",
                    coordinates: [parseFloat(parameters.lng), parseFloat(parameters.lat)]
                },
                zipcode:parameters.zipcode,
                productaddress:parameters.productaddress,
                condition:parameters.condition,
                images:images
            });

            try{
                const productStored = await product.save();

                if (!productStored) {
    
                    return response.status(400).send({
                        status:false,
                        message: `Error registrando producto`,
                    })
                }
            }catch(error){
                return response.status(400).send({
                    status:false,
                    message: `Error registrando producto`,
                    error
                })
            }
           

        }

        return response.status(201).send({ 
            status:true,  
            message: `Producto registrado correctamente`,
        })

    },

    /**
    * @api {get} get-user-products/:id
    * @apiName Products
    * @apiGroup Products
    *
    * Consultar producto por id de usuario
    * 
    * @apiSuccess (201) {Object} mixed `products` object
    */
    getUserProductsProfile: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        let products = await Product.find({ iduser: parameters.iduser, idstate: 1 }).sort({createdate: 'asc'}).
        skip(parseInt(parameters.offset)).limit(parseInt(parameters.limit))

        if (!products) {
            return response.status(200).send({
                status:false,
                message: `Productos no encontrados`,
            })
        } 

        let countProducts = products.length;

        return response.status(200).send({
            status:true,
            countProducts,
            products
        })

    },

    /**
    * @api {get} /find-products
    * @apiName Products
    * @apiGroup Products
    *
    * Consultar productos en un radio 
    * 
    * @apiSuccess (201) {Object} mixed `products` object
    */
    findProducts: async (request, response) => {
       
        const parameters = _.pick(request.body, ['iduser','lat', 'lng', 'category', 'option', 'min_price', 'max_price', 'radius', 'limit', 'offset','search','distance','order']);
        //console.log(parameters);

        //parametros del query
        const query = {
            iduser: { $ne: parameters.iduser }
        }

        //Distancias disponibles
        let distances = {
            0: 5000,
            1: 10000,//10km
            2: 15000,
            3: 20000,
            4: 30000,
            5: 50000,//50km
        }

        //Fitlros de ubicacion
        if(parameters.lat && parameters.lng && parameters.distance ){
            query.location = {
                $near: {
                    $maxDistance: distances[parseInt(parameters.distance)],
                    $geometry: {
                        type: "Point",
                        coordinates: [parseFloat(parameters.lng), parseFloat(parameters.lat)]
                    }
                }
            }
        }
        
        //Filtro por categoria
        if(parameters.category){
            query.idcategory = parameters.category;
        }

        //Filtro por precios >=min Y <=max
        if(parameters.min_price && parameters.max_price ){
            query.$and = [{ 'price': { $gte: parseFloat(parameters.min_price) } }, { 'price': { $lte: parseFloat(parameters.max_price) } }]
        }

        //Filtro por precios >=min
        if(parameters.min_price && parameters.max_price == null ){
            query.$and = [{ 'price': { $gte: parseFloat(parameters.min_price) } }]
        }

        //Filtro por precios <=max
        if(parameters.min_price == null && parameters.max_price ){
            query.$and = [{ 'price': { $lte: parseFloat(parameters.max_price) } }]
        }

        //Buscar por el texto
        if(parameters.search){ 
            query.$or = [{ 'title': { $regex: parameters.search, $options: "i" } }, { 'description': { $regex: parameters.search, $options: "i" } }]
        }

        //default order
        let sort = {createdate: 'desc'};//mas nuevo

        if(parameters.sort){
            if(parameters.sort == 1){//mas cerca
                //mas cerca aplica si el usuario busca la ubicación
                sort = {}
            }else if(parameters.sort == 2){//precio mas bajo
                sort = {price: 'asc'};
            }else if(parameters.sort == 3){//precio mas alto
                sort = {price: 'desc'};
            }
        }
        
        //efectuar consulta de productos
        Product.find(query).sort(sort).skip(parseInt(parameters.offset)).limit(parseInt(parameters.limit))
        .find((error, results) => {
            if (error) console.log(error);

            if(results){
                //console.log('results count',results.length)
                return response.status(201).send({
                    status:true,
                    message: `Consulta exitosa`,
                    results
                })
            }
            return response.status(201).send({
                status:true,
                message: `No hay resultados`,
                results:[]
            })
           
        });
       
    },

    /**
    * @api {get} /find-products
    * @apiName Products
    * @apiGroup Products
    *
    * Consultar detalle de producto 
    * 1. Verifica si esta en algun tablero de usario
    * 2. Añade vista a las estadisticas (numero de visitas de producto)
    * 
    * @apiSuccess (201) {Object} mixed `products` object
    */
    findProductDetail: async (request, response) => {
       
        const parameters = _.pick(request.body, ['idProduct','iduser']);

        //efectuar consulta de detalle de producto y usuario
        let product = await Product.findOne({_id: parameters.idProduct }).populate('iduser', '-token -password')
        .populate('idcategory', 'name');
        
        let countInboard = 0;

        //buscar si el producto esta en algun tablero
        if (typeof parameters.iduser !== 'undefined') {
            
            let boards = await Board.find({iduser: parameters.iduser, products: { "$in" : [parameters.idProduct]}  })

            if(boards){
                countInboard = boards.length;
            }

            //número de visitas de producto
            //buscar el producto en estadisticas con el id de usuario
            let findStats = await StatsProduct.findOne({idproduct: parameters.idProduct, "users.iduser": { "$in" : parameters.iduser} });
            
            //Si no hay registro de visitas al producto se guarda o actualiza
            if(!findStats){

                //1. Actualizar stats
                let statsUpdate = await StatsProduct.findOneAndUpdate({ idproduct: parameters.idProduct },  { $push: {users: {iduser: parameters.iduser} }}, {useFindAndModify: false});

                //2. Si no se logra actualizar se inserta nuevo registro
                if(!statsUpdate){
                    var stats = new StatsProduct({
                        idproduct:parameters.idProduct, 
                        users: {
                            iduser: parameters.iduser,
                        },
                    });
                    await stats.save();
                }

                //3. Actualiza numero de visitas directamente en el producto
                let getStats = await StatsProduct.findOne({ idproduct: parameters.idProduct });
                let updateProduct = await Product.findOneAndUpdate({ _id: parameters.idProduct }, { views: getStats.users.length } , {useFindAndModify: false});
            }

        }

        if(!product){
            return response.status(201).send({
                status:false,
                message: `No hay resultados`,
                product:[],
            })
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            product,
            countInboard
        })
   
    },


}

module.exports = controller;