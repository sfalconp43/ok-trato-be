'use strict'

const { LogMessage } = require('../../models/logMessage');
const { User } = require('../../models/user');

const config = require('../../config/config');

var twilio = require('twilio');

const moment = require('moment');


let controller = {

    /**
    * Enviar SMS de verificacion del numero telefonico del usuario por medio de Twilio
    * @api {post} /send-verification-sms
    * @apiName sendVerificationCodeSms
    * @apiGroup LogMessages
    * @apiParam  {String} [userId] User id
    * @apiParam  {String} [phoneNumber] User's phone number
    * @apiSuccess (201) {Object} mixed object with status and message
    */
    sendVerificationCodeSms: async (request, response) => {
        var client = new twilio(config.TWILIO_ACCOUNT_SID, config.TWILIO_AUTH_TOKEN);
        const code = Math.floor(Math.random() * 1000000);

        //obtener parametros del request
        const parameters = request.body;

        //Enviar mensaje via Twilio
        client.messages
            .create({
                from: '+14156893543',
                to: parameters.phoneNumber,
                body: `El número de verificación para OK Trato es: ${code}`
            })
            .then(async (message) => {

                console.log('Mensaje Enviado Correctamente', message.sid);
                console.log(message);

                // Creacion del LogMessage
                var logMessage = new LogMessage({
                    userId: parameters.userId,
                    phoneNumber: parameters.phoneNumber,
                    verificationCode: code,
                    smsSended: true,
                    response: {
                        from: message.from,
                        to: message.to,
                        smsId: message.sid,
                        status: message.status,
                        smsBody: message.body
                    }
                });

                // Guardado del logMessage en base de datos
                try {
                    const logMessageStored = await logMessage.save();

                    return response.status(201).send({
                        status: true,
                        message: `Mensaje enviado y registrado en la base de datos`,
                        id: logMessageStored._id
                    });
                } catch (error) {
                    console.log(error);
                    return response.status(400).send({
                        status: false,
                        message: `Error guardando el LogMessage`,
                    })
                }
            })
            .catch(async (error) => {

                // Creacion del LogMessage
                var logMessage = new LogMessage({
                    userId: parameters.userId,
                    phoneNumber: parameters.phoneNumber,
                    verificationCode: code,
                    smsSended: false,
                    error: {
                        errorCode: error.code,
                        message: error.message
                    }
                });

                // Guardado del logMessage en base de datos
                try {
                    const logMessageStored = await logMessage.save();

                    return response.status(201).send({
                        status: false,
                        message: `Error al enviar el mensaje al usuario`,
                        id: logMessageStored._id
                    });
                } catch (error) {
                    console.log(error);
                    return response.status(400).send({
                        status: false,
                        message: `Error guardando el LogMessage`,
                    })
                }


            });
    },

    /**
    * Verificar coódigo enviado por SMS
    * @api {post} /verify-code
    * @apiName validateVerificationCode
    * @apiGroup LogMessages
    * @apiParam  {String} [userId] User id
    * @apiParam  {String} [verificationCode] Verification code for validation
    * @apiSuccess (200) {Object} mixed object with status and message
    */
   validateVerificationCode: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //Buscar y comprobar el codigo de verificacion
        try {
            const logMessage = await LogMessage.findOne({ 'userId': parameters.userId, smsSended: true, 'verificationCode': parameters.verificationCode });

            // Logica si se encuentra el logMessage y el codigo de verificacion coincide
            if (logMessage) {

                // Obtener la fecha actual
                var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

                // Campos del usuario a actualizar en la base de datos
                const valuesToUpdate = {
                    phoneNumberChecked: true,
                    phone: logMessage.phoneNumber,
                    updatedate: currentDate
                };

                // Actualizacion del usuario en base de datos
                try {
                    const userUpdate = await User.findOneAndUpdate({ '_id': parameters.userId }, valuesToUpdate, { useFindAndModify: false });

                    return response.status(200).send({
                        status: true,
                        id: userUpdate._id,
                        message: 'Usuario actualizado correctamente',
                    });

                // Logica en caso de error al actualizar la base de datos
                } catch (error) {
                    console.log(error);
                    return response.status(400).send({
                        status: false,
                        message: 'Error al actualizar la base de datos',
                    })
                }
            // Loogica si no se encuentra un logMessage para el usuario
            } else {
                return response.status(400).send({
                    status: false,
                    message: 'Usuario o codigo de verificacion incorrecto',
                })
            }
        // Logica en caso de error al consultar en la base de datos
        } catch (error) {
            return response.status(400).send({
                status: false,
                message: 'Error al consultar la base de datos',
            })
        }
    },


}

module.exports = controller;