'use strict'

const config = require('../../config/config');

const { Category } = require('../../models/category');

//Underscore
const _ = require('underscore');
const moment = require('moment');


let controller = {

    /**
    * @api {get} /getcategories
    * @apiName Categories
    * @apiGroup Categories
    *
    * 
    * Consultar todas las categorias
    * 
    * @apiSuccess (201) {Object} mixed `category` object
    */
    getAllCategories: async (request, response) => {


         //Buscar tableros
        let categories = await Category.find({});
    
        if (!categories) {
            return response.status(400).send({
                status:false,
                message: `Error consultando tableros`,
            })
        }else{
            return response.status(201).send({
                status:true,
                message: `Consulta exitosa`,
                categories
            })
        }

    },

    /**
    * Insertar categoria
    */
    insertCategory: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //registrar categoria 
        var category = new Category({
            name: parameters.name,
            description:parameters.description,
        });

        const categoryStored = await category.save();

        if (!categoryStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando categoria`,
            })
        }

        return response.status(201).send({ 
            status:true,  
            message: `Categoria registrada correctamente`, 
            id: categoryStored._id 
        })

    },

    /**
    * Actualizar categorias
    */
    updateCategories: async (request, response) => {

        const parameters = request.body
        const categoryId = parameters.id;
        const newName = parameters.name;
        const newDescription = parameters.description;

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

        const updateValues = {
            name: newName,
            description: newDescription,
            updateDate: currentDate
        }
        //Buscar y actualizar el/los campo(s)
        let categoryUpdate = await Category.findOneAndUpdate({ _id: categoryId }, updateValues, {useFindAndModify: false});

        if (!categoryUpdate) {
            return response.status(400).send({
                status:false,
                message: `Error actualizando categoria`,
            })
        }

        //retornar respuesta
        return response.status(200).send({
            status:true,
            id: categoryId,
            message: `Categoria actualizada exitosamente`,
        })

    },

}

module.exports = controller;