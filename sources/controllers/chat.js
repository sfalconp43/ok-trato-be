'use strict'

const config = require('../../config/config');
const { Chat } = require('../../models/chat');
const { ChatMsg } = require('../../models/chat_messages');
const { Product } = require('../../models/product');

//Underscore
const _ = require('underscore');

let controller = {

    /**
     * @api {get} /get-messages-chat
     * @apiName Chat
     * @apiGroup Chat
     *
     * Consultar mensajes de chat por namespace y id product
     * 
     * @apiSuccess (201) {Object} mixed `chat` object
     */
    getMessagesNameSpace: async(request, response) => {

        //obtener parametros del request body por medio de underscore
        const data = _.pick(request.body, ['namespace','idproduct','iduser','offset','limit']);

        //Buscar Chat
        let chat = await Chat.findOne({ 
            idproduct: data.idproduct,
            namespace: data.namespace
        })

    
        if (!chat) {
            return response.status(201).send({
                status:false,
                message: `Error consultando chats`,
            })
        }

        //Buscar Chat
        let messages = await ChatMsg.find({ 
            chatid: chat._id,
        }).sort({createdate: 'desc'}).skip(parseInt(data.offset)).limit(parseInt(data.limit));

        //marcar mensajes como leidos 
        if(messages.length>0){
            //actualizar donde: ( chat_id = idChat  and idsender!=mi_id and seen = false)
            let res = await ChatMsg.updateMany({ chatid: chat._id, idsender: { $ne: data.iduser }, seen:false }, { seen: true });   
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            chat,
            messages
        })
        
    },

    
    /**
     * @api {get} /get-messages-chat-id
     * @apiName Chat
     * @apiGroup Chat
     *
     * Consultar mensajes de chat por id del chat
     * 
     * @apiSuccess (201) {Object} mixed `chat` object
     */
    getMessagesById: async(request, response) => {

        //obtener parametros del request body por medio de underscore
        const data = _.pick(request.body, ['idchat','iduser','offset','limit']);

        console.log(data);
        //Buscar Chat
        let chat = await Chat.findOne({ _id: data.idchat })

        if (!chat) {
            return response.status(201).send({
                status:false,
                message: `Error consultando chats`,
            })
        }

        //Buscar Chat
        let messages = await ChatMsg.find({ 
            chatid:  data.idchat,
        }).sort({createdate: 'desc'}).skip(parseInt(data.offset)).limit(parseInt(data.limit))

        //marcar mensajes como leidos 
        if(messages.length>0){
            //actualizar donde: ( chat_id = idChat  and idsender!=mi_id and seen = false)
            let res = await ChatMsg.updateMany({ chatid: chat._id, idsender: { $ne: data.iduser }, seen:false }, { seen: true });   
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            chat,
            messages
        })

    },


    /**
     * @api {post} /insert-chat
     * @apiName Chat
     * @apiGroup Chat
     *
     * Insertar mensaje por namespace 
     * Si ya existe chat se guarda solo el mensaje
     * 
     * El namespace se compone por: ( iduserfrom+iduserto+idproduct )
     * 
     * @apiSuccess (201) {Object} mixed `chat` object
     */
    insertMessage: async(request, response) => {

        //obtener parametros del request body por medio de underscore
        const data = _.pick(request.body, ['iduserfrom', 'iduserto', 'message', 'namespace', 'idproduct','isnew','chatid']);

        //si es nuevo el chat y mensaje
        if(data.isnew){

            //registrar Chat 
            var chat = new Chat({
                iduser1: data.iduserfrom,
                iduser2: data.iduserto,
                namespace: data.namespace,
                idproduct: data.idproduct,
            });

            const chatStored = await chat.save();

            if (!chatStored) {
                return response.status(201).send({status: false, message: `Error guardando chat`});    
            }

            //registrar Mensajes de Chat 
            var chatmsg = new ChatMsg({
                idsender: data.iduserfrom,
                chatid: chat._id,
                message: data.message,
            });

            const chatmsgStored = await chatmsg.save();

            if(!chatmsgStored){
                return response.status(201).send({status: false, message: `Error guardando chat`});   
            }

            return response.status(201).send({ 
                status: true, 
                message: `Chat registrado correctamente`, 
                id: chatmsgStored._id, 
                chat:chatStored,
                createdate: chatmsgStored.createdate
            })


        }else{

            //registrar Mensajes de Chat 
            var chatmsg = new ChatMsg({
                idsender: data.iduserfrom,
                chatid: data.chatid,
                message: data.message,
            });

            const chatmsgStored = await chatmsg.save();

            if(!chatmsgStored){
                return response.status(201).send({status: false, message: `Error guardando chat`});   
            }

            return response.status(201).send({ 
                status: true, 
                message: `Chat registrado correctamente`, 
                id: chatmsgStored._id, 
                createdate: chatmsgStored.createdate
            })

        }

    },

    /**
     * @api {post} /get-list-chat
     * @apiName Chat
     * @apiGroup Chat
     *
     * Obtener listado de chats por prouducto 
     * 
     * @apiSuccess (201) {Object} mixed `messages` object
     */
    getListChatUser: async(request, response) => {

        //obtener parametros del request body por medio de underscore
        const parameters = _.pick(request.body, ['iduser']);

        //el response debe tener:
        //Imagen del producto, imagen del usuario, ultimo mensaje, nombre de usuario
        //hace cuanto tiempo del ultimo mensaje, ultimo mensaje

        //Anadir la ip para contruir la ruta de las imagenes en el array 
        parameters.fullUrl = config.BASE_URL_API;

        let results = await Chat.find({ $or: [{ 'iduser1': parameters.iduser }, { 'iduser2': parameters.iduser }] }).
        populate('iduser1','_id image names lastnames').
        populate('iduser2','_id image names lastnames').
        populate('idproduct').sort({createdate: 'desc'}).lean();

        if(!results){
            return response.status(201).send({
                status:false,
                message: `No hay resultados`,
            })
        }

        let chats = await Promise.all(results.map( async (result) =>{
            //consultar ultimo mensaje y añadirlo al resultado
            let msg = await ChatMsg.findOne({ chatid: result._id }).sort({createdate: 'desc'})
            if(msg){
                result.message = msg.message;
                result.message_date = msg.createdate;
                result.seen = msg.seen;
                result.idsender = msg.idsender;
            }  
           
            return result;
        }));

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            chats
        })

    },

    /**
    * @api {get} /get-products-chat
    * @apiName Chat
    * @apiGroup Chat
    *
    * Obtener productos de los chats activos de un usuario
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    getProductChat: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //id usuario
        let iduser = parameters.iduser;

        //Buscar chats iniciados
        const chats = await Chat.find({ iduser1: iduser }).sort({createdate: 'desc'}).
        skip(parseInt(parameters.offset)).limit(parseInt(parameters.limit));

        if (!chats) {
            return response.status(201).send({
                status:false,
                message: `Error consultando productos`,
            })
        }

        //Obtener ids de productos
        let productsArray = chats.map((chat) => { return chat.idproduct });
        
        //Buscar productos 
        const products = await Product.find({_id: { $in: productsArray }});

        if(!products){
            return response.status(201).send({
                status:false,
                message: `Error consultando productos`,
            })
        }
        
        return response.status(201).send({
            status:true,
            message: `Error consultando productos`,
            products
        })
        
    },



}


module.exports = controller;