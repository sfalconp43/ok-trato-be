'use strict'

const { Board } = require('../../models/board');
const { Product } = require('../../models/product');

//Underscore
const _ = require('underscore');
const moment = require('moment');

let controller = {

    /**
    * @api {get} /get-boards
    * @apiName Board
    * @apiGroup Board
    *
    * Consultar todos los tableros
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    getAllBoards: async (request, response) => {

        //Buscar tableros
        let boards = await Board.find({});
        
        if (!boards) {
            return response.status(400).send({
                status:false,
                message: `Error consultando tableros`,
            })
        }else{
            return response.status(201).send({
                status:true,
                message: `Consulta exitosa`,
                boards
            })
        }
    },

    /**
    * @api {get} /create-board
    * @apiName Board
    * @apiGroup Board
    *
    * Insertar tablero
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    insertBoard: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //registrar tablero 
        var board = new Board({
            iduser: parameters.iduser,
            name:parameters.name,
        });

        const boardStored = await board.save();

        if (!boardStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando tablero`,
            })
        }

        return response.status(201).send({ 
            status:true,  
            message: `Tablero registrado correctamente`, 
            id: boardStored._id 
        })
        
    },

    /**
    * @api {get} /update-board
    * @apiName Board
    * @apiGroup Board
    *
    * Actualizar tablero
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    updateBoards: async (request, response) => {
        const parameters = request.body
        const newName = parameters.name;
        const boardId = parameters.id;

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

        const updateValues = {
            name: newName,
            updatedate: currentDate
        }
        //Buscar y actualizar el campo
        let boardUpdate = await Board.findOneAndUpdate({ _id: boardId }, updateValues, {useFindAndModify: false});

        if (!boardUpdate) {
            return response.status(400).send({
                status:false,
                message: `Error actualizando tablero`,
            })
        }else{
            //retornar respuesta
            return response.status(200).send({
                status:true,
                id: boardId,
                message: `Tablero actualizado exitosamente`,
            })
        }

    },

    /**
    * @api {get} /get-boards-user/:id
    * @apiName Board
    * @apiGroup Board
    *
    * Obtener tableros de usuario
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    getUserBoard: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //Buscar tableros
        const boards = await Board.find({iduser: parameters.iduser}).sort({createdate: 'desc'}).
        skip(parseInt(parameters.offset)).limit(parseInt(parameters.limit)).lean();

        for (var i = 0;i<boards.length;i++){
            
            boards[i].num_products = boards[i].products.length;//cantidad de productos
            boards[i].board_image = null;//imagen de tablero default null

            if(boards.length>0){

                //Si tiene productos el tablero
                if(boards[i].products.length>0){
                    const prodImage  = await Product.find({_id: boards[i].products[0]._id});
                    //Si encuentra el primer producto la primera imagen va a ser 
                    //la imagen del tablero
                    if(prodImage){
                        boards[i].board_image = prodImage[0].images[0];
                    }
                }
                
            } 
        }

        if (!boards) {
            return response.status(400).send({
                status:false,
                message: `Error consultando tableros`,
            })
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            boards, 
        })
        
    },

    /**
    * @api {get} /insert-product-board
    * @apiName Board
    * @apiGroup Board
    *
    * Insertar producto en tablero
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    insertProductBoard: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        var idBoard = parameters.idBoard;
        var idProduct = parameters.idProduct;

        //registrar el producto en el tablero 
        const boardProductStored = await Board.findByIdAndUpdate({_id: idBoard}, 
            { $push: {products: idProduct }},
        );

        if (!boardProductStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando producto en tablero`,
            })
        }

        return response.status(201).send({ 
            status:true,  
            message: `Producto en tablero registrado correctamente`, 
            id: idBoard
        })

    },

    /**
    * @api {get} /delete-board
    * @apiName Board
    * @apiGroup Board
    *
    * Eliminar tablero
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    deleteBoard: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        var idBoard = parameters.idBoard;
        var idUser = parameters.idUser;
    
        const res = await Board.remove({ _id: idBoard, iduser: idUser });
        res.deletedCount; // Number of documents removed

        if(res.deletedCount > 0){

            return response.status(201).send({
                status:true,
                message: `Tablero eliminado exitosamente`,
                idBoard: idBoard
            })
            
        }else{
            return response.status(201).send({
                status:true,
                message: `Tablero eliminado exitosamente`,
            })
        }

    },


    /**
    * @api {get} /get-products-board
    * @apiName Board
    * @apiGroup Board
    *
    * Obtener productos de un tablero de usuario
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    getProductBoard: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //id usuario
        let idUser = parameters.iduser;
        //id tablero
        let idBoard = parameters.idboard;


        //Buscar tableros
        const board = await Board.findOne({_id: idBoard, iduser: idUser});

        if (!board) {
            return response.status(400).send({
                status:false,
                message: `Error consultando tableros`,
            })
        }

        if(board.products.length == 0){
            return response.status(400).send({
                status:false,
                message: `No hay productos en el tablero`,
                products:[]
            })
        }

        //Buscar productos 
        const products = await Product.find({_id: { $in: board.products }}).
        skip(parseInt(parameters.offset)).limit(parseInt(parameters.limit));

        if(!products){
            return response.status(400).send({
                status:false,
                message: `Error consultando productos`,
                products:[]
            })
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            products, 
        })
        
    },

    /**
    * @api {post} /get-boards-user-check
    * @apiName Board
    * @apiGroup Board
    *
    * Obtener tableros de usuario colocando 
    * check donde se encuentre determinado producto
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    getUserBoardWithCheck: async (request, response) => {

        const parameters = request.body;

        //obtener id
        let iduser = parameters.iduser;
        let idproduct = parameters.idproduct;

        //Buscar tableros
        const boards = await Board.find({iduser: iduser}).lean();

        for (var i = 0;i<boards.length;i++){
            
            boards[i].num_products = boards[i].products.length;//cantidad de productos
            boards[i].board_image = null;//imagen de tablero default null

            if(boards.length>0){

                //Si tiene productos el tablero
                if(boards[i].products.length>0){
                    const prodImage  = await Product.find({_id: boards[i].products[0]._id});
                    //Si encuentra el primer producto la primera imagen va a ser 
                    //la imagen del tablero
                    if(prodImage){
                        boards[i].board_image = prodImage[0].images[0];
                    }

                    //Verificar si el producto esta en el board actual
                    let foundProductinBoard = boards[i].products.find(product => product == idproduct);
                    if (foundProductinBoard){
                        boards[i].check = true;  
                    }else{
                        boards[i].check = false;  
                    }
                }else{
                    boards[i].check = false;  
                }
                
            } 
        }

        //buscar si el producto esta en algun tablero
        let countInboard = 0;
        let boardsCount = await Board.find({iduser: iduser, products: { "$in" : [idproduct]}  })

        if(boardsCount){
            countInboard = boardsCount.length;
        }

        if (!boards) {
            return response.status(400).send({
                status:false,
                message: `Error consultando tableros`,
            })
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            boards, 
            countInboard
        })
        
    },

    /**
    * @api {get} /delete-product-board
    * @apiName Board
    * @apiGroup Board
    *
    * Eliminar producto de tablero
    * 
    * @apiSuccess (201) 
    */
    deleteProductFromBoard: async (request, response) => {

        const parameters = request.body;

        //obtener parametros
        let iduser = parameters.iduser;
        let idboard = parameters.idboard;
        let idproduct = parameters.idproduct;

        //Buscar producto en tablero y borrarlo del array
        const boards = await Board.findOneAndUpdate({iduser: iduser, _id:idboard }, { $pull: { products: idproduct } } , {useFindAndModify: false}); 

        if (!boards) {
            return response.status(201).send({
                status:false,
                message: `Error eliminando producto`,
            })
        }

        //retornar respuesta
        return response.status(200).send({
            status:true,
            message: `Producto eliminado`,
        });
        
    },

    /**
    * @api {post} /create-board-product
    * @apiName Board
    * @apiGroup Board
    *
    * Insertar tablero y producto
    * 
    * @apiSuccess (201) {Object} mixed `boards` object
    */
    insertBoardProduct: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //producto
        let products=[];
        products.push(parameters.idproduct);

        //registrar tablero 
        var board = new Board({
            iduser: parameters.iduser,
            name:parameters.name,
            products:products
        });

        const boardStored = await board.save();

        if (!boardStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando tablero`,
            })
        }

        return response.status(201).send({ 
            status:true,  
            message: `Tablero registrado correctamente`, 
            id: boardStored._id 
        })
        
    },



}

module.exports = controller;
