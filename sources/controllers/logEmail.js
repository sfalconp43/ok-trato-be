'use strict'

const { LogEmail } = require('../../models/logEmail');

let controller = {

    /**
    * @api {get} /get-logs-email
    * @apiName LogEmail
    * @apiGroup LogEmail
    *
    * 
    * Consultar todos los logs
    * 
    * @apiSuccess (201) {Object} mixed `LogEmail` object
    */
    getLogEmail: async (request, response) => {

        //Buscar tableros
        let emails = await LogEmail.find({});
    
        if (!emails) {
            return response.status(400).send({
                status:false,
                message: `Error consultando log emails`,
            })
        }else{
            return response.status(201).send({
                status:true,
                message: `Consulta exitosa log emails`,
                emails
            })
        }

    },

    /**
    * Insertar LogEmail
    * @api {get} /insert-log-email
    * 
    */
    insertLogEmail: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //registrar LogEmail 
        var logEmail = new LogEmail({
            iduser: parameters.iduser,
            emailto:parameters.emailto,
            emailfrom:parameters.emailfrom,
            subject:parameters.subject,
            result:parameters.result,
            code:parameters.code,
            emailtype:parameters.emailtype
        });

        const logEmailStored = await logEmail.save();

        if (!logEmailStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando log email`,
            })
        }

        return response.status(201).send({ 
            status:true,  
            message: `log email registrado correctamente`, 
            id: logEmailStored._id 
        })

    },


}

module.exports = controller;