'use strict'

const config = require('../../config/config');

const { Report } = require('../../models/report');

//Underscore
const _ = require('underscore');
const moment = require('moment');
var mongoose = require('mongoose');

let controller = {

    /**
    * @api {get} /get-reports
    * @apiName Report
    * @apiGroup Report
    *
    * 
    * Consultar todas los reportes
    * 
    * @apiSuccess (201) {Object} mixed `Report` object
    */
    getAllReport: async (request, response) => {

         //Buscar reportes
        let report = await Report.find({});
    
        if (!report) {
            return response.status(400).send({
                status:false,
                message: `Error consultando reportes`,
            })
        }else{
            return response.status(201).send({
                status:true,
                message: `Consulta exitosa`,
                report
            })
        }

    },

    /**
    * Insertar reporte
    */
    insertReport: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        const validIdProduct = mongoose.Types.ObjectId.isValid(parameters.idproduct);

        //crear query de consulta
        var query = {
            iduser: parameters.iduser,
            userreported: parameters.userreported,
            type:parameters.type
        };

        //si viene el producto se agrega al query
        if(validIdProduct){
            query.idproduct = parameters.idproduct
        }

        //efectuar consulta de reporte para no insertar duplicados
        let alreadyreported = await Report.findOne(query);

        //si ha sido reportado no hacer nada
        if(alreadyreported){
            return response.status(201).send({
                status:true,
                message: `Ya reportado`,
                reported:1
            })
        }

        //añadir nota
        query.note = parameters.note;
        //añadir id de reporte categoria
        query.idreportcat = parameters.idreportcat;

        //registrar reporte 
        var report = new Report(query);

        const reportStored = await report.save();

        if (!reportStored) {
            return response.status(400).send({
                status:false,
                message: `Error registrando reporte`,
            })
        }

        //reporte registrado
        return response.status(201).send({ 
            status:true,  
            message: `Reporte registrado correctamente`, 
            id: reportStored._id ,
            reported:0
        })

    },

    //Obtener reporte de usuario mediante varios filtros
    getReportUser: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        const validIdProduct = mongoose.Types.ObjectId.isValid(parameters.idproduct);

        //crear query de consulta
        var query = {
            userreported: parameters.userreported
        };

        //si viene el producto se agrega al query
        if(validIdProduct){
            query.idproduct = parameters.idproduct
        }

        if (typeof parameters.type !== 'undefined') {
            query.type = parameters.type
        }

        //Buscar reportes
        let reports = await Report.find(query).populate('userreported', '-token -password').
        populate('idproduct', '-views -condition -updatedate -iduser');
   
        if (!reports) {
            return response.status(201).send({
                status:false,
                message: `Error consultando reportes`,
            })
        }

        return response.status(201).send({
            status:true,
            message: `Consulta exitosa`,
            reports
        })
        

   },

}

module.exports = controller;