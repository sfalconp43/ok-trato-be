'use strict'

const { User } = require('../../models/user');

const fs = require('fs');//archivos
const path = require('path');//rutas
const moment = require('moment');//fechas
const sharp = require('sharp');//tamano imagenes

let controller = {

    /**
    * @api {put} /user-update-image /user-update-cover
    * @apiName actualizar imagen de usuario o portada
    * @apiGroup Upload
    *
    * @apiParam  {int} [id] id usuario
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    updateUserImage: async (request, response, next) => {

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

        var previousImage = null;

        //obtener id de usuario
        let idUser = request.params.id;
        //console.log(idUser)
        //obtener tipo de actualizacion si cover o foto de perfil
        let typeUpdate = request.params.type;

        var folder = 'users';
        if(typeUpdate == 2){
            folder = 'covers';
        }

        //Validar que existe el archivo
        if (!request.file) {
            return response.status(201)
                .send({
                    status: false,
                    message: 'No se ha seleccionado ningún archivo'
                });
        }

        //obtener nombre del archivo cargado
        let fileName = request.file.filename;
        
        const user = await User.findOne({ _id : idUser});
    
        if (!user){
            //eliminar archivo del server
            deleteFile(fileName,folder);

            return response.status(201).send({
                status:false,
                message: `Usuario no econtrado`,
            })
        }

        //obtener imagen o de perfil o de portada segun el caso
        if(typeUpdate == 1){
            previousImage = user.image;
        }else{
            previousImage = user.banner;
        }

        //obtener nombre de imagen original
        let userImage = request.file.originalname;
        let splitName = userImage.split('.');
        //obtener extension del archivo
        let extension = splitName[splitName.length - 1];

        // Extensiones permitidas
        let validEstensions = ['png', 'jpg', 'jpeg'];
        
        //validar correcta extension
        if (validEstensions.indexOf(extension) < 0) {

            //eliminar archivo del server
            deleteFile(fileName,folder);

            return response.status(201).json({
                status:false,
                message: 'Las extensiones permitidas son ' + validEstensions.join(', '),
                ext: extension,  
            })
        }

        //renombrar archivo
        let newName = `${ idUser }-${ new Date().getMilliseconds()  }.${ extension }`;
        renameFile(fileName,newName,folder);

        let updateValuesUser;
        //cambiar de tamano la imagen de acuerdo al tipo
        if(typeUpdate == 1){
            resizeImage(newName,folder,false);
            //actualizar nombre de imagen y fecha de actualizacion en bd
            updateValuesUser = { image: newName, updatedate: currentDate };
        }else{
            resizeImage(newName,folder,true);
            //actualizar nombre de imagen y fecha de actualizacion en bd
            updateValuesUser = { banner: newName, updatedate: currentDate };
        }

        //Buscar y actualizar el campo
        let res = await User.findOneAndUpdate({ _id: idUser },  updateValuesUser , {useFindAndModify: false});

        if(res){
            if(previousImage){
                //console.log('Eliminar imagen anterior');
                //eliminar archivo anterior
                deleteFile(previousImage, folder);
            }
        }

        //retornar respuesta
        return response.status(200).send({
            status:true,
            image:newName,
            id: idUser,
            message: `Imagen de usuario actualizado correctamente id: ${idUser}`,
        })

    }

}

/**
 * Elimina un archivo cargado
 * 
 * @param {String} imageName 
 * @param {String} folder 
 */
function deleteFile(imageName, folder) {
    
    try {
        //obtener ruta de archivo
        let pathImagen = path.resolve(__dirname, `../../public/uploads/${ folder }/${ imageName }`);

        if (fs.existsSync(pathImagen)) {
            fs.unlinkSync(pathImagen);
        }
    } catch (error) {
        console.log(error)
    }

}


/**
 * Renombrar un archivo
 * 
 * @param {String} imageOriginalName 
 * @param {String} imageNewName 
 * @param {String} folder 
 */
function renameFile(imageOriginalName, imageNewName, folder) {
    
    try {
        //obtener ruta de archivo
        let pathImage = path.resolve(__dirname, `../../public/uploads/${ folder }`);

        //renombrar
        fs.renameSync(pathImage+'/'+imageOriginalName, pathImage+'/'+imageNewName, (err)=> {
            if ( err ) 
                console.log('ERROR: ' + err);
        });

    } catch (error) {
            console.log('ERROR: ' + error);
    }

}

/**
 * Cambiar tamano a un archivo cargado
 *
 * @param {String} imageName Nombre de la imagen
 * @param {String} folder Carpeta de imagen
 * @param {Boolean} type Tipo: 0-foto de perfil 1-cover page/portada
 */
function resizeImage(imageName, folder , type) {
    
    //obtener ruta de archivo
    let pathImagen = path.resolve(__dirname, `../../public/uploads/${ folder }/${ imageName }`);

    //nombre parcial 
    let partialname = 'rs-'+imageName;
    //ruta de nuevo archivo
    let pathImagenNew = path.resolve(__dirname, `../../public/uploads/${ folder }/${ partialname }`);

    //tamano imagen perfil
    let h = 500;
    let w = 500;
    if(type){
        //tamano imagen de portada / cover page
        h = 500;
        w = 800;
    }

    //rezise imagen
    sharp(pathImagen).resize({ height: h, width: w }).toFile(pathImagenNew)
    .then((newFileInfo) => {
        //eliminar imagen original y renombrar la que fue modificada de tamano
        deleteFile(imageName, folder);
        renameFile(partialname,imageName,folder);
    })
    .catch((err) => {
        console.log("Error occured resize");
    });

}

module.exports = controller;