'use strict'

//Encriptacion
const bcrypt = require('bcrypt');
const { User } = require('../../models/user');
const { Product } = require('../../models/product');
const config = require('../../config/config');

const e_user_state = require('../enums/e_user_state');
const e_user_role = require('../enums/e_roles');
const e_email_type = require('../enums/e_email_type');

//Enviar email
const Mailer = require('../helpers/sendEmail');

//Underscore
const _ = require('underscore');

//Tokens
const jwt = require('jsonwebtoken');
const moment = require('moment');

let controller = {

    /**
    * @api {post} /login
    * @apiName Login usuario
    * @apiGroup User
    *
    * @apiParam  {String} [email] email
    * @apiParam  {String} [password] password
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    userLogin: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;

        //Buscar email
        let user = await User.findOne({ email: parameters.email });

        if (!user) {
            return response.status(200).send({
                status:false,
                message: `(Usuario) o contraseña incorrectos`,
            })
        } 
        //validar estado de usuario
        if(user.idstate !== 1){
            return response.status(201).send({
                status:false,
                message: `Usuario bloqueado`,
            })
        }

        const validPassword = await bcrypt.compare(parameters.password, user.password);
        if (!validPassword) {
            return response.status(200).send({
                status:false,
                message: `(Usuario) o contraseña incorrectos`,
            })
        }

        //Token solo estos campos
        let userToken = await User.findOne({ _id: user._id }).select("names lastnames email");
        //Generar token jwt
        let newToken = jwt.sign({
            userToken
        }, config.AUTENTICATION_SEED, { expiresIn: config.EXPIRATION_TOKEN });

        //Buscar y actualizar el campo
        let res = await User.findOneAndUpdate({ _id: user._id }, { token : newToken }, {useFindAndModify: false});

        //agregar token al objeto user y retornar los datos del usuario
        user['token'] = newToken;

        //No retornar campos
        user['password'] = 'NA';

        //todo ok
        return response.status(200).send({
            status:true,
            message: `Logged`,
            user
        })

    },


    /**
    * @api {post} /user-register
    * @apiName Registrar usuario
    * @apiGroup User
    *
    * @apiParam  {Int} [idcountry] id pais
    * @apiParam  {String} [names] Nombres
    * @apiParam  {String} [lastnames] Apellidos
    * @apiParam  {String} [phone] Telefono
    * @apiParam  {String} [address] Dirección
    * @apiParam  {String} [birthdate] Fecha de Nacimiento
    * @apiParam  {String} [password] Contrasena
    * @apiParam  {String} [email] Email
    *
    * @apiSuccess (201) {Object} mixed `user` object
    */
    registerUser: async (request, response) => {

        //obtener parametros del request
        const parameters = request.body;
        //encriptar password
        const passwordEscripted = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);

        //registrar usuario moongose
        var user = new User({
            idstate:e_user_state.user_state.REGISTERED,
            idrole:e_user_role.role.CLIENT,
            names:parameters.names,
            lastnames:parameters.lastnames,
            password:passwordEscripted,
            email:parameters.email,
        });

        const userStored = await user.save();

        if (userStored) {

            //SET de parametros para enviar email de confirmación de registro
            let toEmail = userStored.email;
            let subject = `Gracias por usar ${config.APP_NAME} | Confirma tu email`;
            let user = userStored;
            let template = 'confirm_register.ejs';
            let urlAction = `${config.BASE_URL_API}api/v1/accept-register/${userStored._id}`;
            let typeAction = e_email_type.email_type.REGISTER;

            //Enviar email con mailSender
            Mailer.mailSender(toEmail, subject, user, template, urlAction, typeAction );

            //Generar token jwt
            let newToken = jwt.sign({ userStored }, config.AUTENTICATION_SEED, { expiresIn: config.EXPIRATION_TOKEN });

            //Actualizar token en bd
            await User.findOneAndUpdate({ _id: userStored._id }, { token: newToken }, {useFindAndModify: false});

            //agregar token al objeto register y retornar los datos del usuario
            userStored['token'] = newToken;
            //no retornar password
            userStored['password'] = '';

            return response.status(201).send({
                status:true,
                message: `Usuario registrado correctamente`,
                user: userStored
            });

        } else {

            return response.status(400).send({
                status:false,
                message: `Error registrando usuario`,
            })
        }

    },
    

    /**
    * @api {put} /user-update
    * @apiName actualizar datos de usuario por campo
    * @apiGroup User
    *
    * @apiParam  {String} [names] nombres
    * @apiParam  {String} [lastnames] apellidos
    * @apiParam  {String} [phone] telefono
    * @apiParam  {String} [address] direccion
    *
    * @apiSuccess (200) {Object} user id
    */
    updateUserData: async (request, response) => {

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

        let idUser = request.params.id;

        let field = request.params.field;

        let updateValues = _.pick(request.body, [field]);
        
        //Si es password se encripta
        if(field==='password'){
            updateValues.password = await bcrypt.hash(updateValues.password, config.SALT_ROUNDS_BCRYPT);
        }

        //Agregar fecha de actualización
        updateValues.updatedate = currentDate;

        //Buscar y actualizar el campo
        let user = await User.findOneAndUpdate({ _id: idUser }, updateValues, {useFindAndModify: false});

        if (!user) {
            return response.status(400).send({
                status:false,
                message: `Usuario no encontrado`,
            })
        } 

        let userUpdated = await User.findOne({ _id: idUser });

        if (userUpdated) {
            //retornar respuesta
            return response.status(200).send({
                status:true,
                id: idUser,
                message: `Usuario actualizado correctamente id: ${idUser}`,
            })
        } 

    },


    /**
    * @api {post} /check-email
    * @apiName checkEmail
    * @apiGroup User
    *
    * Buscar email durante el login para verificar si esta registrado
    * 
    * @apiParam  {String} [email] email
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    checkEmail: async (request, response) => {

        //obtener parametros del request body por medio de underscore
        const data = _.pick(request.body, ['email']);
        
        //Buscar email / no retornar password
        let user = await User.findOne({ email: data.email }).select("-password");

        if (!user) {
            return response.status(201).send({
                status:true,
                register:false,
                message: `Usuario no registrado`,
            })
        }
        
        if(user.idstate == e_user_state.user_state.BLOCKED){
            return response.status(201).send({
                status:false,
                register:true,
                message: `Usuario bloqueado`,
                user,
            })
        }
        
        return response.status(201).send({
            status:true,
            register:true,
            message: `Usuario registrado`,
            user,
        })
        
    },


    /**
    * @api {post} /update-password
    * @apiName updateUserPassword
    * @apiGroup User
    *
    * Actualizar password de usuario
    * 
    * @apiParam  {String} [password] password
    * @apiParam  {String} [oldPassword] password anterior
    * @apiParam  {String} [confirmPassword] password confirmacion
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    updateUserPassword: async (request, response) => {

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

        const parameters = request.body;

        let idUser = parameters.id;

        let field = parameters.password;

        let updateValues = _.pick(request.body, [field]);
        
        updateValues.password = await bcrypt.hash(parameters.password, config.SALT_ROUNDS_BCRYPT);

        let user = await User.findOne({ _id: idUser });

        if (!user) {
            return response.status(400).send({
                status:false,
                message: `Usuario no econtrado`,
            })
        }

        const match = await bcrypt.compare(parameters.oldPassword, user.password);

        //Si hace match los password
        if(match) {
            //si coinciden se comparan los nuevos password y se actualizan
            if (parameters.password === parameters.confirmPassword) {

                updateValues.updatedate = currentDate;

                //Buscar y actualizar el campo
                let userUpdate = await User.findOneAndUpdate({ _id: idUser }, updateValues, {useFindAndModify: false});

                if (!userUpdate) {
                    return response.status(400).send({
                        status:false,
                        message: `Error actualizando password`,
                    })
                }else{
                    return response.status(200).send({
                        status:true,
                        id: idUser,
                        message: `Password actualizado correctamente id: ${idUser}`,
                    })
                }
                
            }else{
                //retornar respuesta
                return response.status(200).send({
                    status:false,
                    code:2,
                    message: `Contraseñas no coiciden`,
                    error
                })
            }
        }else{
            return response.status(200).send({
                status:false,
                code:1,
                message: `Contraseña Incorrecta`
            })
        }

    },

    /**
    * @api {get} /accept-register
    * @apiName updateUserState
    * @apiGroup User
    *
    * Actualizar estado de usuario a confirmado
    * 
    * @apiParam  {String} [id] user id
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    updateUserState: async (request, response) => {

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");

        let idUser = request.params.id;

        const updateValues = {
            emailchecked : true,
            updatedate: currentDate
        }

        let user = await User.findOneAndUpdate({ _id: idUser }, updateValues, {useFindAndModify: false});

        if (!user) {
            return response.status(400).send({
                status:false,
                message: `Usuario no encontrado`,
        })
        }else{
            return response.status(201).render('accept-register', {
                status:true,
                message: `Estado actualizado exitosamente`,
                user: idUser
        })
    }

},

    /**
    * @api {get} /send-confirm-email
    * @apiName sendConfirmationEmail
    * @apiGroup User
    *
    * Enviar email de confirmacion de registro
    * 
    * @apiParam  {String} [id] user id
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    sendConfirmationEmail: async (request, response) => {

        let idUser = request.params.id;

        let user = await User.findOne({ _id: idUser });

        if (!user) {
            return response.status(400).send({
                status:false,
                message: `Usuario no encontrado`,
            })
        }else{

            let toEmail = user.email;
            let subject = `Gracias por usar ${config.APP_NAME} | Confirma tu email`;
            let template = 'confirm_register.ejs';
            //let template = 'output2.html';
            let urlAction = `${config.BASE_URL_API}api/v1/accept-register/${user._id}`;
            let typeAction = e_email_type.email_type.REGISTER;

            //Llamado al helper
            Mailer.mailSender(toEmail, subject, user, template, urlAction, typeAction ).then((result)=>{

                if(!result){
                    return response.status(200).send({
                        status:false,
                        message: `Email no enviado enviado`,
                    })
                }

                return response.status(200).send({
                    status:true,
                    message: `Email enviado satisfactoriamente`,
                })
                
            }).catch((err)=>{
                return response.status(400).send({
                    status:true,
                    message: `Email no enviado enviado`,
                    err
                })
            });

        }

    },
    /**
    * @api {post} /update-facebook
    * @apiName updateStatusFacebook
    * @apiGroup User
    *
    * Relizar conexión con Facebook
    * 
    * @apiParam  {String} [id] user id
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    updateStatusFacebook: async (request, response) => {

        var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");
        
        let parameters = request.body;

        let idUser = parameters._id;

        let idFacebook =  parameters.idfacebook

        console.log('id user', idUser, 'id facebook', idFacebook);

        const updateValues = {
            idfacebook : idFacebook,
            updatedate: currentDate,
            facebookchecked: true
        }
    
        let user = await User.findOneAndUpdate({ _id: idUser }, updateValues, {useFindAndModify: false});

        if (!user) {
            return response.status(400).send({
                status:false,
                message: `Usuario no encontrado`,
            })
        }else{

            return response.status(200).send({
                status:true,
                message: `Se ha conectado exitosamente a Facebook`,
                user: idUser
            })
        }
    },

    /**
    * @api {get} /get-user-data
    * @apiName getUserDetails
    * @apiGroup User
    *
    * Obtener data de cliente para perfil
    * 
    * @apiParam  {String} [id] user id
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    getUserDetails: async (request, response) => {

        const parameters = request.body;

        let user = await User.findOne({ _id: parameters.iduser }).select('-password -token -idrole');

        if (!user) {
            return response.status(200).send({
                status:false,
                message: `Usuario no encontrado`,
            })
        } 
        
        if(user.idstate !== 1){
            return response.status(201).send({
                status:false,
                message: `Usuario bloqueado`,
            })
        }

        let countProducts = await Product.countDocuments({ iduser: parameters.iduser, idstate: 1 })

        return response.status(200).send({
            status:true,
            user,
            countProducts
        })
    
    },

    /**
    * @api {get} /get-user-products
    * @apiName getUserProducts
    * @apiGroup User
    *
    * Obtener productos de cliente para perfil
    * 
    * @apiParam  {String} [id] user id
    *
    * @apiSuccess (200) {Object} mixed `user` object
    */
    getUserProducts: async (request, response) => {
        
        const parameters = request.body;

        let products = await Product.find({ iduser: parameters.iduser, idstate: 1 }).sort({createdate: 'desc'}).
        skip(parseInt(parameters.offset)).limit(parseInt(parameters.limit))

        if (!products) {
            return response.status(200).send({
                status:false,
                message: `Productos no encontrados`,
            })
        } 

        let countProducts = products.length;

        return response.status(200).send({
            status:true,
            countProducts,
            products
        })

    },
    

}

module.exports = controller;