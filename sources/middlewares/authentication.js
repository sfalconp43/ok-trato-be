const jwt = require('jsonwebtoken');
const config = require('../../config/config');
/*
* Verificar Token
*/
let verifyToken = (request, response, next) => {

    let token = request.get('token');

    jwt.verify(token, config.AUTENTICATION_SEED, (error, decoded) => {

        if (error) {
            return response.status(401).json({
                status: false,
                message: 'Unauthorized',
            });
        }

        //registrar en el request el usuario autenticado
        request.user = decoded.user;
        next();

    });

};


module.exports = {
    verifyToken
}