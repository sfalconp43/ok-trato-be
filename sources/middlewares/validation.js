const { check, validationResult } = require('express-validator');

/*
* Verificar datos de registro
*/
let verifyUserData = async (request, response, next) => {

    await check('names').not().isEmpty().run(request);
    await check('lastnames').not().isEmpty().run(request);
    await check('password').isLength({ min: 6 }).run(request);
    await check('email').not().isEmpty().run(request);
    
    const result = validationResult(request);
    if (!result.isEmpty()) {
        return response.status(422).send({ 
			status:false,
			message: `Error de validación de campos`,
			errors: result.array() 
		});
    }

    next();

};

/*
* Verificar datos de actualizacion
*/
let verifyUserUpdateData = async (request, response, next) => {

    //obtener field
    let field = request.params.field;

    console.log('Validando datos de actualizacion');
    //SOLO  lo requerido
    await check(field).not().isEmpty().run(request);

    /*await check('names').not().isEmpty().run(request);
    await check('lastnames').not().isEmpty().run(request);
    await check('phone').not().isEmpty().run(request);
    await check('address').not().isEmpty().run(request);
    await check('birthdate').not().isEmpty().run(request);*/
    
    const result = validationResult(request);
    if (!result.isEmpty()) {
        return response.status(422).send({ 
			status:false,
			message: `Error de validación de campos`,
			errors: result.array() 
		});
    }

    next();

};

module.exports = {
	verifyUserData,
	verifyUserUpdateData
}