module.exports = {

    condition: {
        OTHER: 1,//Otro
        REPARATION: 2,//Para reparación
        USED:3,//Usado
        OPEN_BOX:4,//Caja Abierta (nunca usado)
        REFURBISHED: 5,//Reacondicionado/Certificado
        NEW: 6//Nuevo
    },
    
}