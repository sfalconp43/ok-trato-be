module.exports = {

    product_state: {
        ACTIVE: 1,
        SOLD: 2,
        SET_ASIDE: 3,
        RECLAMATION: 4,
    },
}