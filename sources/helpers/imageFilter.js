const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Solo archivos de imagenes son permitidos!';
        return cb(new Error('Solo archivos de imagenes son permitidos!'), false);
    }
    cb(null, true);
};
exports.imageFilter = imageFilter;