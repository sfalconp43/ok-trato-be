"use strict";

const nodemailer = require("nodemailer");
const config = require("../../config/config");
var ejs = require("ejs");
const path = require('path');//rutas
const { LogEmail } = require('../../models/logEmail');
//Underscore
const _ = require("underscore");

const mailSender = async (toEmail, subject, user, template, urlAction, typeAction) => {


    return new Promise((resolve, reject) => {

        let fromEmail = config.EMAIL; 
        let APP_NAME = config.APP_NAME; 
        let result = false;
        let transporter = nodemailer.createTransport({
            host: config.HOST,
            port: config.PORT,
            secure: config.SECURE,
            auth: {
                user: config.auth.USER, 
                pass: config.auth.PASS
            },
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false
            },
        });
        
        //Se configura el mensaje con los parametros que vienen del controlador users
        let mailRoute = path.resolve(__dirname, `../../views/emailTemplates/`);
    

        try{ 
            ejs.renderFile (`${mailRoute}/${template}`, { name: user.names, urlAction, APP_NAME }, (err, data) => { //renderizar en una variable el template del correo
    
                if (err) {
                    console.log(err);
                    resolve(false);
                }
                console.log('1: ',result)
                let mainOptions = {
                    from: `${APP_NAME} ${fromEmail}`,
                    to: toEmail,
                    subject : subject,
                    text: subject,
                    html: data
                };
        
                transporter.sendMail(mainOptions, (err,info)=> {
                    if (err) {
                        console.log(err);
                        //registrar LogEmail 
                        var logEmail = new LogEmail({
                            iduser: user._id,
                            emailto:toEmail,
                            emailfrom:fromEmail,
                            subject:subject,
                            result:'failed',
                            code:err.code,
                            emailtype:typeAction
                        });
                        logEmail.save();
                        resolve(false);
                    }else{
                        console.log('Message sent: ' + info.response);
                        //registrar LogEmail 
                        var logEmail = new LogEmail({
                            iduser: user._id,
                            emailto:toEmail,
                            emailfrom:fromEmail,
                            subject:subject,
                            result:info.response,
                            code:info.response,
                            emailtype:typeAction
                        });
                        logEmail.save(); 
                        resolve(true);
                    }
        
                })
              
            });
        }catch{
            reject(false);
        }
        
    });
};

exports.mailSender = mailSender;
