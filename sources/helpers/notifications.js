'use strict'
const { Expo } = require('expo-server-sdk');
const { Notification } = require('../../models/notification');



module.exports = {
    sendNotification: async (title, message, type, objectId, userToken) => {
        // crear un cliente de Expo SDK
        let expo = new Expo();

        // Creacion del notification
        var notification = new Notification({
            title: title,
            message: message,
            type: type,
            objectId: objectId,
        });

        // Guardado de la notificacion y generacion de la notificacion push
        try {
            // Guardar la notificacion en base de datos
            const notificationStored = await notification.save();

            // Array de mensajes
            let messages = [];

            // Se llena el array de mensajes, con solo un elemento en este caso
            if (Expo.isExpoPushToken(userToken)) {
                messages.push({
                    to: userToken,
                    sound: 'default',
                    title: title,
                    body: message,
                });

                // Se declara y se llena el bloque de notificaciones a enviar, un solo elemento en este caso
                let chunks = expo.chunkPushNotifications(messages);

                // Array para el resultado del envio de notificaciones push
                let tickets = [];

                // Se envian las notificaciones push. Ademas, se obtiene y retorna el resultado del envio 
                for (let chunk of chunks) {
                    try {
                        let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                        tickets.push(...ticketChunk);

                        // Se guardo la notificacion en base de datos y se envio la notificacion push
                        return ({
                            status: true,
                            message: 'Notificacion generada con exito (Se envio la notificacion push).',
                            tickets: tickets
                        });
                    } catch (error) {
                        // Se guardo la notificacion en base de datos, pero no se envio la notificacion push
                        return ({
                            status: false,
                            message: 'Notificacion guardada en base de datos (No se envio la notificacion push).',
                            error: error
                        });
                    }
                }
            } else {
                // El token parara la notificacion push es invalido
                return ({
                    status: false,
                    message: 'Token des usuario para la notificacion push invalido.',
                });
            }
        } catch (error) {
            // No se pudo guargar la notificacion en base de datos y no se envio la notificacion push
            return ({
                status: false,
                message: 'No se pudo guardar la notifocacion en base de datos (No se envio la notificacion push).',
                error: error
            });
        }
    },

    sendMultipleNotifications: async (titles, messages, types, objectsIds, usersTokens) => {
        // crear un cliente de Expo SDK
        let expo = new Expo();
        let notifications = [];
        let counterInvalidTokens = 0;

        // Creacion de las notifications
        for (let index = 0; index < objectsIds.length; index++) {
            const title = titles[index];
            const message = messages[index];
            const type = types[index];
            const objectId = objectsIds[index];

            var notification = new Notification({
                title: title,
                message: message,
                type: type,
                objectId: objectId,
            });

            notifications.push(notification);
        }

        // Guardado de las notificaciones y generacion de la notificaciones push
        try {
            // Guardar las notificaciones en base de datos
            const notificationsStored = await Notification.insertMany(notifications);

            // Array de mensajes
            let messages = [];

            // Se llena el array de mensajes
            for (let index = 0; index < objectsIds.length; index++) {
                const title = titles[index];
                const message = messages[index];
                const userToken = usersTokens[index];

                if (Expo.isExpoPushToken(userToken)) {
                    messages.push({
                        to: userToken,
                        sound: 'default',
                        title: title,
                        body: message,
                    });
                } else {
                    counterInvalidTokens++;
                }
            }

            // Se declara y se llena el bloque de notificaciones a enviar, un solo elemento en este caso
            let chunks = expo.chunkPushNotifications(messages);

            // Array para el resultado del envio de notificaciones push
            let tickets = [];

            // Se envian las notificaciones push. Ademas, se obtiene y retorna el resultado del envio 
            for (let chunk of chunks) {
                try {
                    let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                    tickets.push(...ticketChunk);

                    // Se guardaron lsa notificaciones en base de datos y se enviaron las notificaciones push
                    return ({
                        status: true,
                        message: 'Notificaciones generadas con exito (Se enviaron las notificaciones push).',
                        counterInvalidTokens: counterInvalidTokens,
                        tickets: tickets
                    });
                } catch (error) {
                    // Se guardaron las notificaciones en base de datos, pero no se enviaron las notificaciones push
                    return ({
                        status: false,
                        message: 'Notificaciones guardadas en base de datos (No se enviaron las notificaciones push).',
                        error: error
                    });
                }
            }
        } catch (error) {
            // No se pudo guargar las notificaciones en base de datos y no se enviaron las notificaciones push
            return ({
                status: false,
                message: 'No se pudo guardar las notificaciones en base de datos (No se enviaron las notificaciones push).',
                error: error
            });
        }
    },

}