'use strict'

const express = require('express');
const router = express.Router();
const phoneValidateController = require('../controllers/logMessage');

//Envia sms de verificación
router.post('/send-verification-sms/', phoneValidateController.sendVerificationCodeSms);

//Confirmar codigo de validación
router.post('/verify-code/', phoneValidateController.validateVerificationCode);

module.exports = router;