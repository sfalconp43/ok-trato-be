'use strict'

const express = require('express');
const router = express.Router();
const UsersController = require('../controllers/user');

//middlewares
const { verifyUserData, verifyUserUpdateData } = require('../middlewares/validation');
const { verifyToken } = require('../middlewares/authentication');

//Login usuario
router.post('/login', UsersController.userLogin);
//Registrar usuario
router.post('/user-register', verifyUserData, UsersController.registerUser);
//Actualizar usuario
router.put('/user-update/:id/:field',[verifyToken, verifyUserUpdateData], UsersController.updateUserData);
//Verificar email registrado
router.post('/check-email/', UsersController.checkEmail);
//Actualizar password
router.post('/update-password/',[verifyToken], UsersController.updateUserPassword);
//Confirmar registro
router.get('/accept-register/:id', UsersController.updateUserState);
//Enviar email de confirmacion de registro
router.post('/send-confirm-email/:id', UsersController.sendConfirmationEmail);
//Confirmar conexión con facebook
router.post('/update-facebook', UsersController.updateStatusFacebook);
//Obtener data de cliente para perfil
router.post('/get-user-data', UsersController.getUserDetails);
//Obtener productos de cliente para perfil
router.post('/get-user-products', UsersController.getUserProducts);


module.exports = router;