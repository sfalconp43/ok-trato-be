'use strict'

const express = require('express');
const router = express.Router();
const logEmailController = require('../controllers/logEmail');

//Obtener categorias de productos
router.get('/get-logs-email/', logEmailController.getLogEmail);
//Insertar categorias de productos
router.post('/insert-log-email/', logEmailController.insertLogEmail);

module.exports = router;