'use strict'

const express = require('express');
const router = express.Router();
const ChatController = require('../controllers/chat');

//Consultar mensajes de chat por namespace y id product
router.post('/get-messages-chat/', ChatController.getMessagesNameSpace);

//Consultar mensajes de chat por id del chat
router.post('/get-messages-chat-id/', ChatController.getMessagesById);

//Obtener lista de chats activos
router.post('/get-list-chat/', ChatController.getListChatUser);

//Insertar mensaje por namespace 
//Si ya existe chat se guarda solo el mensaje
router.post('/insert-chat/', ChatController.insertMessage);

//Obtener productos de los chats activos
router.post('/get-products-chat/', ChatController.getProductChat);

module.exports = router;