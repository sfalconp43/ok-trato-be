'use strict'

const express = require('express');
const router = express.Router();
const ProductsController = require('../controllers/products');
const multer = require('multer');
const helpers = require('../helpers/imageFilter');
// storage user cover
var storageProducts = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/products/')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + Date.now())
    }
})

var upload = multer({ storage: storageProducts, fileFilter: helpers.imageFilter })

//Insetar producto 
router.post('/insert-product/', upload.array('productimages',14) ,ProductsController.insertProduct);

//Obtener productos de usuario
router.post('/get-user-products-profile/', ProductsController.getUserProductsProfile);

//Consultar productos cercanos
router.post('/find-products/', ProductsController.findProducts);

//Consultar detalle de producto
router.post('/find-product-detail/', ProductsController.findProductDetail);

module.exports = router;