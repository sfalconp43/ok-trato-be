'use strict'

const express = require('express');
const router = express.Router();
const CategoriesController = require('../controllers/categories');

//Obtener categorias de productos
router.get('/get-categories/', CategoriesController.getAllCategories);
//Actualizar categorias de productos
router.post('/update-categories/', CategoriesController.updateCategories);
//Insertar categorias de productos
router.post('/insert-category/', CategoriesController.insertCategory);

module.exports = router;