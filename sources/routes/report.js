'use strict'

const express = require('express');
const router = express.Router();
const CategoriesController = require('../controllers/report');

//Obtener reportes
router.get('/get-reports/', CategoriesController.getAllReport);
//Insertar categorias de productos
router.post('/insert-report/', CategoriesController.insertReport);
//Insertar categorias de productos
router.post('/get-report-by-user/', CategoriesController.getReportUser);

module.exports = router;