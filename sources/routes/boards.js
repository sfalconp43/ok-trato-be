'use strict'

const express = require('express');
const router = express.Router();
const BoardsController = require('../controllers/boards');

//Obtener todos los tableros
router.get('/get-boards/', BoardsController.getAllBoards);
//Insertar tablero
router.post('/create-board', BoardsController.insertBoard);
//Actualizar tablero
router.post('/update-board/', BoardsController.updateBoards);
//Obtener tablero de usuarios
router.post('/get-boards-user/', BoardsController.getUserBoard);
//Insertar Producto en tablero
router.post('/insert-product-board/', BoardsController.insertProductBoard);
//Eliminar tablero
router.post('/delete-board/', BoardsController.deleteBoard);
//Obtener productos de tablero
router.post('/get-products-board/', BoardsController.getProductBoard);
//Obtener productos de tablero (home)
router.post('/get-boards-user-check/', BoardsController.getUserBoardWithCheck);
//Obtener productos de tablero (home)
router.post('/delete-product-board/', BoardsController.deleteProductFromBoard);
//Insertar tablero y producto 
router.post('/create-board-product/', BoardsController.insertBoardProduct);
module.exports = router;