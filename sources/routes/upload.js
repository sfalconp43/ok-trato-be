'use strict'

const express = require('express');
const router = express.Router();
const uploadController = require('../controllers/upload');

const multer = require('multer');

// storage user
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/users/')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + Date.now())
    }
})

// storage user cover
var storageCovers = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/uploads/covers/')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + Date.now())
    }
})

var upload = multer({ storage: storage })

var uploadCover = multer({ storage: storageCovers })

//middlewares
//verificar token
const { verifyToken } = require('../middlewares/authentication');

//Actualizar imagen de usuario
router.post('/user-update-image/:id/:type', [verifyToken, upload.single('userimage')] , uploadController.updateUserImage);

//Actualizar imagen de portada
router.post('/user-update-cover/:id/:type', [verifyToken, uploadCover.single('userimage')] , uploadController.updateUserImage);

module.exports = router;