'use strict'

const express = require('express');
const router = express.Router();
const ReportCategoriesController = require('../controllers/reportCategory');

//Obtener categorias de reportes
router.post('/get-report-categories/', ReportCategoriesController.getReportCategory);
//Insertar categorias de reportes
router.post('/insert-report-category/', ReportCategoriesController.insertCategoryReport);

//Insertar categorias de reportes
router.post('/insert-batch/', ReportCategoriesController.seedCategoryReport);

module.exports = router;