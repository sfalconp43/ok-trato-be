'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const socketIO = require('socket.io');
const http = require('http');
const config = require('./config/config');

const app = express();

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//permisos carpetas
app.use('/users', express.static('public/uploads/users/'));
app.use('/covers', express.static('public/uploads/covers/'));
app.use('/products', express.static('public/uploads/products/'));
app.use('/assets', express.static('public/assets'));
app.set('view engine', 'ejs');

// Rutas
var userRoutes = require('./sources/routes/user');
var userBoards = require('./sources/routes/boards');
var Categories = require('./sources/routes/categories');
var Products = require('./sources/routes/products');
var userImages = require('./sources/routes/upload');
var logEmail = require('./sources/routes/logEmail');
var Chat = require('./sources/routes/chat');
var logMessage = require('./sources/routes/logMessage');
var report = require('./sources/routes/report');
var reportCategory = require('./sources/routes/reportCategory');

// app.use('/api/v1', methodRoutes);
app.use('/api/v1', userRoutes);
app.use('/api/v1', userBoards);
app.use('/api/v1', Categories);
app.use('/api/v1', Products);
app.use('/api/v1', userImages);
app.use('/api/v1', logEmail);
app.use('/api/v1', Chat);
app.use('/api/v1', logMessage);
app.use('/api/v1', report);
app.use('/api/v1', reportCategory);

app.use('/', userRoutes);

//set const global
app.locals.APP_NAME = config.APP_NAME;

let server = http.createServer(app);

const io = socketIO.listen(server);

//Escuchar conexiones al socket
io.on("connection", socket => {

	//entrar a un namespace
	socket.on('msgc', function(namespace) {
		//console.log('joining namespace', namespace);
		socket.join(namespace);
	});

	//entrar a un namespace de lista de chats
	socket.on('clist', function(namespace) {
		//console.log('joining namespace list', namespace);
		socket.join(namespace);
	});
	
	//enviar mensajes a un namespace en especifico
	socket.on('send message', function(data) {
		//console.log('sending namespace post', data.room);
		socket.broadcast.to(data.room).emit('message', {
			_id: data.id,
			idsender: data.idsender,
			message: data.message,
			createdate:data.createdate
		});
	});

	//enviar alerta de nuevo mensaje en la lista de chats
	socket.on('send alert', function(data) {

		socket.broadcast.to(data.room).emit('list chat', {
			message: data.message,
			idchat: data.chatid,
			createdate:data.createdate
		});
	});
	
});

// Elemplo de envio de notificacion push
const notification = require('./sources/helpers/notifications');

let aux = notification.sendNotification('TITULO','este es el mensaje',2,'5e7331f7dd838247b8528218','ExponentPushToken[oMhWZMO0J7xtipj6B9oOps]');
aux.then((result) => {
	console.log(result);
})

let aux2 = notification.sendMultipleNotifications(['TITULO 1', 'TITULO 2'],['Mensaje 1', 'Mensaje 2'],[1, 2],['5e7331f7dd838247b8528218', '5e7331f7dd838247b8528218'],['ExponentPushToken[oMhWZMO0J7xtipj6B9oOps]', 'PushToken[oMhWZMO0J7xtipj6B9oOps]']);
aux2.then((result) => {
	console.log(result);
})



// Exportar
module.exports = server;