'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Board = mongoose.model('Board', new mongoose.Schema({
    iduser: {
        type: 'ObjectId', 
        ref: 'User' 
    },
    active: {
        type: Boolean,
        required: true,
        default: true,
    },
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 100
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: false,
    },
    products:  [ {type: Schema.Types.ObjectId, ref: 'Product'} ],
}));
 
exports.Board = Board;