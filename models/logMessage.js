'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LogMessage = mongoose.model('LogMessage', new Schema({
    userId: {
        type: 'ObjectId', 
        ref: 'User' ,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    verificationCode: {
        type: String,
        required: true
    },
    smsSended: {
        type: Boolean,
        required: true,
    },
    response: {
        type: Object,
        default: null,
    },
    error: {
        type: Object,
        default: null,
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now,
    },
    updatedate: {
        type: Date,
        required: true,
        default: Date.now,
    },
}));

exports.LogMessage = LogMessage;