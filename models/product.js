'use strict'

const mongoose = require('mongoose');
const moment = require('moment');
const e_product = require('../sources/enums/e_product_state');
const Schema = mongoose.Schema;
const config = require('../config/config');

var ProductSchema = new Schema({
    iduser: {
        type: 'ObjectId', 
        ref: 'User' 
    },
    idcategory: {
        type: 'ObjectId', 
        ref: 'Category' 
    },
    idstate: {
        type: Number,
        required: true,
        default: e_product.product_state.ACTIVE,
    },
    title: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
    },
    description: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 300
    },
    price: {
        type: Number,
        required: true,
    },
    discount: {
        type: Number,
        required: false,
    },
    flexible:{
        type: Boolean,
        required: true,
    },
    location: {
        type: { type: String },
        coordinates: []
    },
    zipcode: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 20
    },
    productaddress : {
        type: String,
        required: true,
        minlength: 1,
    },
    views: {
        type: Number,
        required: false,
        default:0
    },
    condition : {
        type: Number,
        required: true,
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: false,
    },
    images: [
        { type: String },
    ],
});

//crear index para location
ProductSchema.index({ location: "2dsphere" });

//imagen de portada
ProductSchema.virtual('URI').get(function() {
    return config.BASE_URL_API+this.images[0];
});

ProductSchema.set('toObject', { virtuals: true })
ProductSchema.set('toJSON', { virtuals: true })

const Product = mongoose.model('Product', ProductSchema);
 
exports.Product = Product;