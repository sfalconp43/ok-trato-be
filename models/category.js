'use strict'

const mongoose = require('mongoose');
const moment = require('moment');
const Category = mongoose.model('Category', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50
    },
    description: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: false,
    }
}));
 
exports.Category = Category;