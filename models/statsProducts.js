'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StatsProduct = mongoose.model('StatsProduct', new Schema({
    idproduct: {
        type: 'ObjectId', 
        ref: 'Product' 
    },
    users:  [ 
        {
            iduser: {
                type: 'ObjectId', 
                ref: 'User' 
            },
            createdate: {
                type: Date,
                default: Date.now
            },
        },
    ],
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: false,
    },
}));
 
exports.StatsProduct = StatsProduct;