'use strict'

const mongoose = require('mongoose');
const moment = require('moment');

const User = mongoose.model('User', new mongoose.Schema({
    idstate: {
        type: Number,
        required: true,
    },
    idrole: {
        type: Number,
        required: true,
    },
    names: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 100
    },
    lastnames: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 100
    },
    phone: {
        type: String,
        required: false,
        minlength: 5,
        maxlength: 50
    },
    address: {
        type: String,
        required: false,
        minlength: 5,
        maxlength: 300
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    image: {
        type: String,
        required: false,
        default: null
    },
    banner: {
        type: String,
        required: false,
        default: null
    },
    token: {
        type: String,
        required: false,
    },
    emailchecked:{
        type: Boolean,
        required: false,
        default: false,
    },
    idfacebook: {
        type: String,
        required: false,
    },
    facebookchecked: {
        type: Boolean,
        required: false,
        default: false,
    },
    phoneNumberChecked: {
        type: Boolean,
        required: false,
        default: false,
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: false,
    }
}));
 
exports.User = User;