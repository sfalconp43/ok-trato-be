'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatMsg = mongoose.model('ChatMsg', new Schema({
    idsender: {
        type: 'ObjectId', 
        ref: 'User' 
    },
    chatid: {
        type: 'ObjectId', 
        ref: 'Chat' 
    },
    message: {
        type: String,
        required: true,
    },
    seen:{
        type: Boolean,
        required: false,
        default: false
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    }
}));
 
exports.ChatMsg = ChatMsg;