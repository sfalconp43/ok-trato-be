'use strict'

const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const Chat = mongoose.model('Chat', new Schema({
    iduser1: {
        type: 'ObjectId', 
        ref: 'User' 
    },
    iduser2: {
        type: 'ObjectId', 
        ref: 'User' 
    },
    idproduct: {
        type: 'ObjectId', 
        ref: 'Product' 
    },
    namespace: {
        type: String,
        required: true,
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    }
}));
 
exports.Chat = Chat;