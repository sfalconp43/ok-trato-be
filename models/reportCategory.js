'use strict'

const mongoose = require('mongoose');
const ReportCategory = mongoose.model('ReportCategory', new mongoose.Schema({
    
    description: {
        required: true,
        type: String,
    },
     //tipo de reporte por usuario o producto 0:usuario/1:producto
    type:{
        type: Number,
        required: true,
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
}));
 
exports.ReportCategory = ReportCategory;