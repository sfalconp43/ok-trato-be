'use strict'

const mongoose = require('mongoose');
const Report = mongoose.model('Report', new mongoose.Schema({
    iduser: {
        required: true,
        type: 'ObjectId', 
        ref: 'User' 
    },
    userreported: {
        required: true,
        type: 'ObjectId', 
        ref: 'User' 
    },
    idproduct: {
        type: 'ObjectId', 
        ref: 'Product', 
    },
    idreportcat: {
        type: 'ObjectId', 
        ref: 'ReportCategory' 
    },
    //tipo de reporte por usuario o producto 0:usuario/1:producto
    type:{
        type: Number,
        required: true,
        default:0,
    },
    note: {
        type: String,
        maxlength: 300
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
}));
 
exports.Report = Report;