'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Notification = mongoose.model('Notification', new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 100
    },
    message: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 400
    },
    type: {
        type: Number,
        required: true,
    },
    objectId: {
        type: Schema.Types.ObjectId
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: true,
        default: Date.now
    }
}));
 
exports.Notification = Notification;