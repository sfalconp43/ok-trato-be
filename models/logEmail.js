'use strict'

const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const LogEmail = mongoose.model('LogEmail', new Schema({
    iduser: {
        type: 'ObjectId', 
        ref: 'User' 
    },
    emailto: {
        type: String,
        required: true
    },
    emailfrom: {
        type: String,
        required: true
    },
    subject: {
        type: String,
        required: true
    },
    result: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    emailtype: {
        type: Number,
        required: true,
    },
    createdate: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedate: {
        type: Date,
        required: false,
    },
}));
 
exports.LogEmail = LogEmail;